from time import sleep
import numpy as np
import gym

# the environment is
e = gym.make("FrozenLake-v0")
iCount = e.observation_space.n
aCount = e.action_space.n

# let's initialize the Qtable
Qtable = {}
for i in range(iCount):
    Qtable[i] = np.random.rand(aCount)

# setting the hyperparameters
lr = 0.3
lrMin = 0.00099
lrDecay = 0.99999
gamma = 1.0
epsilon = 1.0
epsilonMin = 0.001
epsilonDecay = 0.95
trial = 1250

# Train
for i in range(trial):
    print("trial {}/{}".format(i + 1, trial))
    s = e.reset()
    done = False

    while not done:
        if np.random.random() < epsilon:
            a = np.random.randint(0, aCount)
        else:
            a = np.argmax(Qtable[s])

        newS, r, done, _ = e.step(a)
        Qtable[s][a] = Qtable[s][a] + lr * (
            r + gamma * np.max(Qtable[newS]) - Qtable[s][a]
        )
        s = newS

        if lr > lrMin:
            lr *= lrDecay

        if not r == 0 and epsilon > epsilonMin:
            epsilon *= epsilonDecay


print("")
print("Learning Rate :", lr)
print("Epsilon :", epsilon)

# Test
print("\nPlaying for 100 trials...")
avg_r = 0
for i in range(100):
    s = e.reset()
    done = False
    while not done:
        a = np.argmax(Qtable[s])
        newS, r, done, _ = e.step(a)
        s = newS

    avg_r += r / 100.0

print("Average reward on 100 trial :", avg_r)
